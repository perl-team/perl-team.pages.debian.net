=head1 Using git tags for packaging

=head2 DISCLAIMER

Please, be advised that the supported and recommended way to package a Perl
distribution under the Debian Perl team is documented in our
L<Git guide|https://perl-team.pages.debian.net/git.html>.

=head2 DESCRIPTION

This document explains a way of packaging a Perl project from the upstream git
tags instead of the CPAN tarballs. See
L<libgit-annex-perl|https://salsa.debian.org/perl-team/modules/packages/libgit-annex-perl>
for an actual example.

=head2 STEPS

=over 4

=item * Check out a new branch starting at upstream release tag.

=item * Make the package barebones inside a dzil build:

    dzil build
    cd Foo-Bar-*
    dh-make-perl --pkg-perl

=item * Move F<debian/> upwards to the actual repository:

    mv debian ..
    cd ..

The rest of the output resulting from dh-make-perl is discarded, including the
git repository created inside the dzil build.

=item * Replace F<debian/rules> with:

    #!/usr/bin/make -f

    %:
    	dh $@ --with dist-zilla

=item * Replace F<debian/watch> with a non-CPAN version.

For instance, if upstream is using cgit:

    version=4
    https://git.example.com/Foo-Bar \
        /snapshot/Foo-Bar-v?@ANY_VERSION@@ARCHIVE_EXT@

=item * Add B<dzil> and B<dzil plugins> to build-deps.

For instance, F<debian/control> may have the following contents depending on
the plugins enabled in dzil F<dist.ini>:

    Build-depends:
     debhelper-compat (= 12),
     dh-dist-zilla,
     libdist-zilla-plugin-git-perl,
     libdist-zilla-plugin-podweaver-perl,

=item * Enable autopkgtest in F<debian/control>:

    Testsuite: autopkgtest-pkg-perl

=item * You may also want a F<debian/tests/pkg-perl/use-name> like this:

    Foo::Bar

Unless the main module name can be obtained from F<META.yml> or F<META.json>.

=item * Commit changes to F<debian/> and clean:

    git add debian
    git commit
    dzil clean

=item * Produce orig.tar using git-archive:

    git deborig

=item * Now you should be able to B<dpkg-buildpackage>.

=back

=head1 Authors

=over

=item * Sean Whitton

=item * Alex Muntada

=back

=head1 License

Copyright (c) 2020 by the individual authors and contributors noted
above.  All rights reserved.  This document is free software; you may
redistribute it and/or modify it under the same terms as Perl itself

Perl is distributed under your choice of the GNU General Public License version
1 or later or the Artistic License.  On Debian systems, the complete text of
the GNU General Public License version 1 can be found in
`/usr/share/common-licenses/GPL-1' and the Artistic License in
`/usr/share/common-licenses/Artistic'.

=for comment
vim:tw=79
