=encoding utf8

=head1 NAME

policy - Debian Perl Group Policy

=head1 INTRODUCTION

Project Internal Policy

This document describes how we do stuff internally. It is of course work in
progress and probably always will be.

We are using git repositories to keep the packages under
revision control. Details of working with these repositories are outside
the scope of this document;  it is recommended that you read the Debian
Perl Group Git Guide (L<https://perl-team.pages.debian.net/git.html>).

=head1 INDEX

=over 4

=item 1. L<Mandatory mailing lists and such|/"Mandatory mailing lists and such">

=item 2. L<debianE<sol>changelog handling and versioning|/"debian/changelog handling and versioning">

=item 3. L<debianE<sol>control handling|/"debian/control handling">

=item 4. L<debianE<sol>copyright handling|/"debian/copyright handling">

=item 5. L<debianE<sol>watch handling|/"debian/watch handling">

=item 6. L<Package Naming Policy|/"Package Naming Policy">

=item 7. L<Test suites|/"Test suites">

=item 8. L<Embedded third-party modules in incE<sol>|/"Embedded third-party modules in inc/">

=item 9. L<Security Hardening Build Flags for XS Modules|/"Security Hardening Build Flags for XS Modules">

=item 10. L<Debian Maintainers practice|/"Debian Maintainers practice">

=item 11. L<Release Process|/"Release Process">

=item 12. L<Dual-lived Modules|/"Dual-lived Modules">

=item 13. L<AUTHORS|/"AUTHORS">

=item 14. L<LICENSE|/"LICENSE">

=back

=head1 Mandatory mailing lists and such

All members of our group should be subscribed to debian-perl@lists.debian.org
and read this list (at least skim all messages for Debian Perl Group related
stuff). Additionally, every member should be subscribed to
pkg-perl-maintainers@lists.alioth.debian.org to receive bug reports and similar information.

All members are encouraged to install and use our packages. They are also
encouraged to check our git repositories
(L<https://salsa.debian.org/perl-team/modules>). You can watch the work on our
git repositories via gitlab's watch feature. The commit messages are also sent
to #debian-perl at irc.debian.org.

=head1 debian/changelog handling and versioning

We use the debian revision to count our releases to the debian archive, not
internal steps. So if and only if you do the first change after a release, you
add another debian/changelog entry (dch -i).  Note that the name and email
address in the debian/changelog entry (i.e. after --) should be present in
Uploaders: in debian/control (otherwise lintian will think that you
are doing an NMU unless the first line is C<* Team upload>).

If you change something that has to be noted in debian/changelog, add a
line to the current entry (dch -a). The [firstname lastname] markers added by
dch are okay to give credit to non-upload-permitted contributors (also for the
initial changelog entry).

Important NOTES to other group members may be placed at the top of the current
changelog entry of packages that are not yet ready for upload (e.g. why a
package is still UNRELEASED etc.).

=head1 debian/control handling

When pushing a package's git repository for the first time change the
Maintainer field to "Debian Perl Group
E<lt>pkg-perl-maintainers@lists.alioth.debian.orgE<gt>" and 
put your own email
address and name in the Uploaders field to show that you feel responsible
for the package.

When you make a significant modification to a package in the repository, add your
name to the Uploaders field. You should keep the names of other contributors who
added their names before you.

The packages maintained by the group should contain the following fields:

=over 

=item Section

Perl packages should be uploaded to the I<perl> section.

=item Priority

Most Perl packages should be of priority I<optional>.

=item Maintainer

All group-maintained packages list
I<"Debian Perl Group E<lt>pkg-perl-maintainers@lists.alioth.debian.orgE<gt>">
as the maintainer.

=item Uploaders

Contains the list of contributors to the specific package, i.e. persons
interested in co-maintaining it in the future.

=item Build and runtime dependencies

We try to support backporting our packages to oldstable/stable where
possible. This affects handling of versioned (build) dependencies.

=over

=item *

Regarding dependency versions this means adding versions when a package with
an older version is still in oldstable or stable. Versions that are already
fulfilled by the oldest release in the archive should be left out.

=back

At some point (usually a year after a release) oldstable will be removed
from the archive; at that point support for backporting to oldstable becomes
moot.

Helpful tools: "B<rmadison> I<package>" for seeing the package releases in all
archive areas; "B<corelist -a> I<Module::Name>" to find out when a dual-lifed
module was integrated into perl core.

We try to support bootstrapping efforts, so if build dependencies are
exclusively needed for the build tests, they should be annotated with
I<E<lt>!nocheckE<gt>>.

=item Vcs-Git

An URL pointing to the package's git repository. That 
is, for a package called I<libsomething-perl>,
C<https://salsa.debian.org/perl-team/modules/packages/libsomething-perl.git>.

=item Vcs-Browser

An URL pointing to the public Web interface where this package's base location
in the repository can be browsed. For I<libsomething-perl>, it would be
C<https://salsa.debian.org/perl-team/modules/packages/libsomething-perl>.

=item Homepage

The URL for this module's upstream homepage. For CPAN modules, unless the
author has a specific homepage, you can point to
L<https://metacpan.org/release/Something>.

=item Testsuite

If the package uses the L<autopkgtest framework|https://perl-team.pages.debian.net/autopkgtest.html>,
a Testsuite field with the value I<autopkgtest-pkg-perl> should be added.

=item Rules-Requires-Root

Perl packages normally don't need root to build so the
Rules-Requires-Root field should be set to I<no> unless it's really
needed, in which case it should be set to an appropriate value, see
the Debian policy at
L<https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-rules-requires-root>.

=back

The other usual fields should, of course, be present and have sensible values. 
In particular, try to stick to the highest published Standards-Version and 
a modern enough debhelper compatibility level. Typically we would use

 Build-Depends: debhelper-compat (= 13)

in F<debian/control> and no F<debian/compat> file. 

The pkg-perl group members prefer to directly use debhelper and B<dh(1)> when packaging. We
suggest you don't use cdbs as part of the build process for packages intended
to be group-maintained.

=head1 debian/copyright handling

Each package should have the copyright file F<debian/copyright> following the
copyright-format 1.0 or newer. copyright-format 1.0 as released together with
Debian policy 3.9.3 is documented at
L<https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/>.

The most recent copyright-format specification is referenced in Debian policy
under L<https://www.debian.org/doc/debian-policy/ch-docs.html#s-copyrightformat>.

=head1 debian/watch handling

Each package should have a watch file (F<debian/watch>) to make manual and
automatic checking for new upstream versions easier. The recommended format
for a watch file for a typical module I<libfoo-bar-perl> hosted on CPAN is:

 version=4
 https://metacpan.org/release/Foo-Bar   .*/Foo-Bar-v?@ANY_VERSION@@ARCHIVE_EXT@$

=head1 Package Naming Policy

Where possible, packages should be named in a consistent manner, per the
L<Debian Perl Policy|https://www.debian.org/doc/packaging-manuals/perl-policy/>,
section 4.2: Module Package Names.

Packages are named differently depending on whether their primary purpose
is considered to be a B<library> or an B<application>. The Comprehensive
Perl Archive Network (CPAN) is the upstream source of the great majority
of our packages. CPAN's notion of a "distribution" is roughly compatible
with Debian's notion of a "package", and so, packages are generally named
after the distributions they came from.

=head2 Libraries

Suppose there is a hypothetical package called I<Widget::Factory>, which is
a library that exposes an interface for other Perl programs. This package
also contains a well-known application called I<widgetmaker>. In this case,
we should:

=over 4

=item 1.

Have a source package called I<libwidget-factory-perl>

=item 2.

Produce a single binary package: B<libwidget-factory-perl>

=item 3.

Set up the B<libwidget-factory-perl> binary package so that it also:

  Provides: widgetmaker

=back

Some consideration is required when other packages may place dependencies
on the application component of a package. In this
case, we can use a versioned Provides.

=head2 Applications

Occasionally, we encounter Perl distributions that are intended to be used
as standalone applications, rather than libraries. These distributions are
frequently, but not always, in the App:: namespace. Developers must use
their own judgment to decide if this is the case, but if there is any doubt,
the issue should be discussed on the mailing list.

Suppose there is a hypothetical distribution called I<Robot::Maker>, which
is a suite of software assisting with the design of robot, best known by
its users as I<robotmaker>. Since this is an application, we should:

=over 4

=item 1.

Produce a source package called I<robotmaker>

=item 2.

Produce a binary package called B<robotmaker>

=item 3.

If other libraries depend on the interface provided by library components
in this package, but these files are distributed in the same upstream
distribution, then we should also produce a binary package called:
B<librobot-maker-perl> or similar, which will be a dependency of the
B<robotmaker> package.

=back

=head1 Test suites

CPAN distributions typically come with a test suite that is run at build time.
As a general rule, we want to run as many tests as possible in order to catch errors.
Some details need to be taken into account:

=over 4

=item *

We can't run tests that need internet access.
If they are run by default they must be disabled (if not controlled by a
specific environment variable, a patch is required).

=item *

We don't enable tests that are explicitly declared as upstream/author/release tests by the author
(typically via an environment variable as I<RELEASE_TESTING>, I<AUTHOR_TESTING>, or I<TEST_AUTHOR>).

Tests designed to be run in automatic builds/installs (usually enabled by setting
I<AUTOMATED_TESTING>) are fine.

=item *

Some tests are, even if not declared explicitly as release tests, a bit fragile and tend
to break with new releases of the test modules, or test style and not functionality
(Test::Perl::Critic, Test::Spelling, Test::Kwalitee, ...). It might be helpful to avoid running them.

=item *

Tests that are disabled or not enabled (like the ones needing internet
access) should be run manually by the person preparing the package on their
local machine before uploading.

=item *

Tests that need display access usually work with B<xvfb>:

    debian/control:
    Build-Depends(-Indep): ..., xvfb, xauth, ...

    debian/rules:

=begin html

<pre>override_dh_auto_test:
	xvfb-run -a dh_auto_test</pre>

=end html

=item *

Some tests need versioned build dependencies for dual-lifed modules (e.g. B<Test::More>),
cf. L<Build and runtime dependencies|/"Build and runtime dependencies">.

=back

=head1 Embedded third-party modules in inc/

Some distributions ship embedded modules in inc/, e.g as part of the build
system or for testing. While it is OK to have own code in inc/ to extend an
existing build system (e.g. B<Module::Build>), an embedded B<third-party>
module used for building or testing shouldn't be used but instead packaged
in Debian and added as a C<Build-Depends{,-Indep}> entry, while at the same
time making sure the embedded version isn't used during package build.

There are some exceptions to this rule, currently:

=over 4

=item *

Packages using B<inc::latest> can continue to use it, but must provide
versioned C<Build-Depends{,-Indep}> accordingly to make sure the embedded
version isn't used.

=item *

Using B<Modules::Install> in Debian packages has so far proven mostly
harmless and is considered acceptable; replacing the embedded fragments with
the packaged C<libmodule-install-perl> doesn't always work, but we expect
that for a new package or when upgrading to the lastest upstream version the
packager would at least try to use the packaged version.

=item *

B<Alien::*> modules are currently considered problematic in general but
acceptable as build dependencies, as they prove difficult to remove and
require a lot of fiddling with the build system, which would probably amount
to maintaining a fork of the module.

=back

=head2 Example of handling inc/ at build time

Say a package includes libdevel-checklib-perl in inc/.
We should make sure the embedded version isn't used, and
set appropriate build-depends:


    debian/control:
    Build-Depends: ..., libdevel-checklib-perl, ...

    debian/rules:

=begin html

<pre># we want to use the system version of included modules, so
# we move the inc away and move it back afterwards
override_dh_auto_clean:
	dh_auto_clean
	[ ! -d $(CURDIR)/inc.save ] || mv $(CURDIR)/inc.save $(CURDIR)/inc

override_dh_auto_configure:
	[ ! -d $(CURDIR)/inc ] || mv $(CURDIR)/inc $(CURDIR)/inc.save
	dh_auto_configure</pre>

=end html

=head1 Security Hardening Build Flags for XS Modules

B<XS Modules> (i.e using arch:any) should use B<hardening build flags>.

As a rule, we enable all hardening flags by setting C<DEB_BUILD_MAINT_OPTIONS>
to C<hardening=+all>:

    debian/rules:
    export DEB_BUILD_MAINT_OPTIONS = hardening=+all

However, some hardening flags can cause issues on some architectures, or break
the build altogether.

In that case, it is acceptable to enable only a subset of flags for the package:

    debian/rules:
    export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

Please consider re-enabling all the flags vi C<+all> whenever there is a new
upstream release in the module or a newer GCC version in Debian.

=head1 Debian Maintainers practice

The Debian project has adopted the Debian Maintainers (DM) concept (cf.
L<https://www.debian.org/vote/2007/vote_003>) in Summer 2007. The pkg-perl group
doesn't see this approach fit for its workflow and its use is discouraged.

=head1 Release Process

If you are a DD, upload but be prepared to receive (at least part of) the blame. If
you are not, some DD in the Group will surely sponsor the package. They will
check the package first, too, but make sure there is no reason to complain.  If
you have a package ready for upload, ask at debian-perl@lists.debian.org
or in the channel #debian-perl at irc.debian.org in case noone picks up the
ready-for-upload package from any of L<PET|https://pet.debian.net/pkg-perl/pet.cgi> (past),
L<DMD|https://udd.debian.org/dmd/?email1=pkg-perl-maintainers%40lists.alioth.debian.org>
(present), or L<tracker|https://tracker.debian.org/teams/pkg-perl/> (future).

Always feel free to ask others to check a package if in doubt.

=head1 Dual-lived Modules

Dual-lived modules (those which are available separately from CPAN as well as in
perl itself) can be packaged separately in Debian if and only if the version
available on CPAN is greater than that in core, and there is a need for that
version in Debian.

When Depending on such packages, declare the package name and version as normal
(perl now supports versioned Provides, so separate packages will only be pulled
in if needed).

When perl is updated with newer dual-lived modules versions, the separately
packaged modules become obsolete. Such packages should not be shipped in a
stable release, and should be removed from the archive if there has been
no upstream release within two years.

=head1 VERSION

2021-08-21

=head1 AUTHORS

=over

=item * Joachim Breitner

=item * Daniel Ruoso

=item * Gustavo Franco

=item * Gunnar Wolf

=item * Gregor Herrmann

=item * Jonathan Yu

=item * Dominic Hargreaves

=item * Clément Hermann

=back


=head1 SOURCE

The canonical source of this document can be found at:
L<https://salsa.debian.org/perl-team/perl-team.pages.debian.net/-/blob/master/policy.pod>.

=head1 LICENSE

Copyright (c) 2004-2020 by the individual authors and contributors noted
above.  All rights reserved. This document is free software; you may
redistribute it and/or modify it under the same terms as Perl itself

Perl is distributed under your choice of the GNU General Public
License or the Artistic License.  On Debian GNU/Linux systems, the
complete text of the GNU General Public License can be found in
`/usr/share/common-licenses/GPL' and the Artistic License in
`/usr/share/common-licenses/Artistic'.
