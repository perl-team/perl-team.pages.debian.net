(Unofficial) Debian Perl Sprint 2022
====================================

Introduction
------------

3 members of the Debian Perl Group met in Hamburg between May 23 and May 30
2022 as part of the [Debian Reunion Hamburg] and conducted an "unofficial"
sprint, i.e. worked on our packages and tools without prior announcement and
planning.

The participants would like to thank the [Debian Reunion Hamburg] organizers
for providing the framework for our sprint, all sponsors of the event, and
all donors to the Debian project who helped to cover parts of our expenses.

[Debian Reunion Hamburg]: https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg

Summary
-------

* Tooling
    - new tool: *dpt-prepare*: prepare yourself and a source code directory
      before working on a package
    - new tool: *dpt-fixup*: apply some automatic fixes to packaging
    - new module: *Debian::PkgPerl::Util*: utility functions for PkgPerl
      tools, currently provides *probable_build_dependency()*
    - new library: *dpt-lib.sh*: shell library for dpt-* scripts
    - new module: *Debian::Upstream::Metadata::Perl*: create
      *debian/upstream/metadata* (used by *dh-make-perl* and
      *dpt-debian-upstream*)
    - *dh-make-perl*: new option *--guess-nocheck* to add <!nocheck>
    - various small improvemens in *dh-make-perl* and *dpt-\**
* Packaging
    - Uploads: ≈ 52
    - Bugs (fixed, triaged, …): ≈33
    - Packages with autopkgtests enabled: quite a few :)
    - RM candidates: some identified/pinged, a few RM bugs filed
* Misc
    - discussions on branch names in Git (DEP-14 and git's master→main
      change)

Details
-------

The full log is available in [Gobby].

[Gobby]: https://gobby.debian.org/export/Teams/Perl/Team-Sprint-Hamburg-2022
