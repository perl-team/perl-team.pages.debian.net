Title: (unofficial) Debian Perl Sprint 2022
Slug: debian-perl-sprint-2022
Date: 2022-07-15 13:37
Author: gregor herrmann
Tags: perl, sprint, hh2022
Status: draft


Three members of the [Debian Perl Group](https://wiki.debian.org/Teams/DebianPerlGroup)
met in Hamburg between May 23 and May 30 2022 as part of the 
[Debian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg)
to continue perl development work for Bookworm and to work on QA tasks across
our 3800+ packages.

The participants had a good time and met other Debian friends. The
sprint was also productive:

* pkg-perl-tools and dh-make-perl were improved and extended.
* More than 50 uploads were done, and more than 30 bugs were fixed or at least triaged.
* autopkgtests were added to lots of packages.
* Some requests to remove obsolete packages were filed as well.

The more detailed [report](https://lists.debian.org/debian-perl/2022/07/msg00009.html)
was posted to the Debian Perl mailing list.

The participants would like to thank the Debian Reunion Hamburg organizers
for providing the framework for our sprint, all sponsors of the event, and
all donors to the Debian project who helped to cover parts of our expenses.
