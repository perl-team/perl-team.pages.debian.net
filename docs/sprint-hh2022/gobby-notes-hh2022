# Inofficial Debian Perl Group Sprint @ DebianReunionHH, Hamburg May 2022


## Participants

* dam
* emollier
* gregoa


## Log of activity

### Sunday, 22 May

### Monday, 23 May

#### Uploads

* libterm-choose-perl
* libpod-parser-perl
* libobject-pad-fieldattr-final-perl
* libobject-pad-fieldattr-isa-perl
* libobject-pad-fieldattr-lzayinit-perl
* libobject-pad-fieldattr-trigger-perl
* libhtml-widgets-navmenu-perl
* libsvn-look-perl
* libfile-xdg-perl
* libcgi-application-plugin-authentication-perl

#### Bugs

* #1010115: prepare patch in git
* #963727: reply
* #986907: close
* #1006658: close
* #964155: fix with a patch
* #995402 (libclass-dbi-sweet-perl): fixed with a patch and uploaded
* #950999: upload with (old) patch

#### Potential RM candidates

* #961147 libcolor-calc-perl
  #963433 libcolor-calc-perl - no (more) rdeps, mail sent
* #959193 libperl5i-perl - low popcon no rdeps: GO! mail sent
* #994736 libmath-gsl-perl → still open
  #993323 libmath-gsl-perl - 1 buildrdep: libmath-matrix-maybegsl-perl (optional; missing at runtime)
* #998351 libdbd-sqlite2-perl - no (more) rdeps, mail sent

#### Other work

* make plans :)
* go through some RC bugs and some dh-make-perl bugs
* work on perlimports / #1001176 RFP


### Tuesday, 24 May

#### Bugs

* #952229: upload with versioned build dependency
* #984112: upload workaround (-std=gnu++11)
* #1006386: pinged upstream issue
* #1009031: pinged upstream issue
* 
* #905679: close
* #905650: close
* #905603: confirm segfault still present on bullseye and bookworm/sid
* #987197: close
* #991102: close
* #641922: close
* #743559: close
* #860737: close
* #1010241: close
* #968871: close
* #813766: close
* #541086: close
* #904479: close
* #1011725: close

#### Uploads

* libmail-dkim-perl
* libunicode-map-perl
* libalien-gnuplot-perl
* liblogger-syslog-perl
* libnet-daemon-perl
* libsys-gamin-perl
* libparallel-prefork-perl
* dh-make-perl
* libio-async-ssl-perl
* libjson-perl

#### Other work

* pkg-perl-tools improvements
* first thoughts on git branch names (DEP-14 and master→main)


### Wednesday, 25 May

#### Bugs

* #489184: close
* #962407 restart discussion

#### Uploads

* perlimports
* libjson-pp-perl
* libcpan-perl-releases-perl
* libtest-www-mechanize-perl
* libstring-tagged-perl
* dh-make-perl × 2

#### Other work

* ping bugs of potential RM candidates
* dh-make-perl: split upstream metadata handling in Debian::Upstream::Metadata::Perl
* dh-make-perl: tools/bump-version-after-release added
* added utility module for determining whether a given package is likely to be just used in tests and thus can be annotated with <!nocheck>
* dh-make-perl: --guess-nocheck option added


### Thursday, 26 May

#### Bugs

* #1011820

#### Uploads

* pkg-perl-tools with dpt-debian-upstream using Debian::Upstream::Metadata::Perl
* libclang-perl / #1000924
* libmath-bigint-perl
* libtest-redisserver-perl

* speed-up matching between module name and package name when discovering dependencies (-1s per dependency)
  
#### Other work

* start a shell library for pkg-perl-tools
* add dpt-prepare to pkg-perl-tools
* triage #841701 (closed)

### Friday, 27 May

#### Bugs

* #993323 patched
* #993323 fix attempted

#### Uploads

* libmojolicious-perl
* prolix (autopkgtest enabled and fixed)
* libpoe-component-sslify-perl
* libmason-perl (autopkgtest enabled)
* libmason-plugin-cache-perl (autopkgtest enabled)
* libmason-plugin-htmlfilters-perl (autopkgtest enabled)
* libmason-plugin-routersimple-perl (autopkgtest enabled)
* libclass-makemethods-perl (autopkgtest enabled)
* libmath-gsl-perl

#### Other work

* add dpt-fixup to pkg-perl-tools; release pkg-perl-tools

### Saturday, 28 May

#### Uploads

* librose-object-perl
* libstring-trim-perl
* liblwp-useragent-chicaching-perl
* libtest-diaginc-perl
* libmath-gsl-perl × 2
* libdancer2-plugin-passphrase-perl
* liblingua-identify-perl
* libmce-perl
* libfile-localizenewlines-perl
* libmath-bigint-gmp-perl
* libmodule-corelist-perl

#### Bugs

* #1004699: apply upstream patch
* #994736: another attempt, closed on the third

#### Other work

* RM bugs filed for libdbd-sqlite2-perl and libperl5i-perl
* fix colors-in-colors in dpt-lib.sh

### Sunday, 29 May

#### Uploads

#### Bugs

* #923829 reproduced, marked as forwarded

#### Other work

* small tweaks to pkg-perl-tools

## Brainstorming for "Sprint"

- forward RC bugs upstream, ping upstream tickets

- git branch name (git warns about 'master' via dh-make-perl) - DEP14
  BoF 2021
  what to change to? DEP-14: debian/unstable or something
  tools: dh-make-perl; dpt(-salsa, takeover)? mrconfig?
  convert local repos

- Bug#962407 http-tiny and SSL verification -- merge bugs in dependent packages?
