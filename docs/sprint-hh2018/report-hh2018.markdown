Debian Perl Sprint 2018
=======================

Introduction
------------

3 members of the Debian Perl Group met in Hamburg between May 16 and May 20
2018 as part of the [Mini-DebConf Hamburg] to continue perl development work
for Buster and to work on QA tasks across our 3500+ packages.  The preparation
details can be found on the [sprint wiki].

The participants would like to thank the [Mini-DebConf Hamburg] organizers for
providing the framework for our sprint, [CSC] for sponsoring one of the
attendees, and all donors to the Debian project who helped to cover a large
part of our expenses.

[Mini-DebConf Hamburg]: https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg
[sprint wiki]: https://wiki.debian.org/Sprints/2018/DebianPerlSprint
[CSC]: https://www.csc.fi/en/


Bugs and Packages
=================

Overview
--------

Bugs [tagged] with:

* user: debian-perl@lists.debian.org
* usertags: hh2018

A total of 28 bugs were filed/worked on. These include:

* newly filed: 20 bugs
    - incl. 5 fixed during the sprint
    - incl. 2 RoM bug to drop unmaintained upstream software
* resolved: 9 bugs
    - incl. 7 bugs filed during the sprint

[tagged]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=hh2018;users=debian-perl@lists.debian.org

Some details
------------

* Bugs around *YAML*: work on regressions, ping the security related bugs,
  upload new upstream release.
    - [#862373]\: libyaml-libyaml-perl: Unconditionally instantiates objects
      from yaml data
    - [#862475]\: libyaml-syck-perl: Unconditionally instantiates objects from
      yaml data
    - [#898561]\: libmarc-transform-perl: FTBFS with libyaml-perl >= 1.25-1
      (test failures)
    - [#898578]\: libyaml-perl/1.25-1 breaks rex/1.6.0-1 test suite (FTBFS and
      autopkgtest failure)
* Versioned Provides:
    - [#867081]\: autopkgtest: @ no longer pulls in packages with versioned
      Provides
* CI regressions:
    - [#898977]\: libnet-dns-zonefile-fast-perl: FTBFS: You are missing
      required modules for NSEC3 support
* Perl 5.28 transition related (see below)

[#862373]: https://bugs.debian.org/862373
[#862475]: https://bugs.debian.org/862475
[#867081]: https://bugs.debian.org/867081
[#898561]: https://bugs.debian.org/898561
[#898578]: https://bugs.debian.org/898578
[#898977]: https://bugs.debian.org/898977

Perl 5.27/5.28
==============

src:perl
--------

*perl* 5.27.11 was packaged and uploaded to [perl.debian.net] to allow
for some initial QA prior to the release of perl 5.28. Some
notable details include:

* Started first test rebuilds, see the [Perl-5.28-QA] gobby document.
* Improved performance of rebuild process (disable *lintian* and add
  *eatmydata*).
* Updated the transition scripts in the `pkg-perl/scripts` repo.
* Bugs filed and/or fixed:
    - [#898946]\: sbuild: --make-binNMU should imply --no-arch-all
    - [#898955]\: dist: Please import new upstream snapshot for metaconfig -X
      support
    - [#898989]\: altree: FTBFS with Perl 5.27: _quicksort sub-pragma removed
    - [#898994]\: texinfo: Unescaped left brace in regex is deprecated, FTBFS
      with Perl 5.27/5.28
    - [#899017]\: libprotocol-acme-perl: FTBFS with newer versions of
      ExtUtils::MakeMaker: cannot remove README.pod
    - [#899075]\: libmonitoring-icinga2-client-rest-perl: FTBFS with newer
      versions of ExtUtils::MakeMaker: cannot remove README.pod
    - [#899110]\: perl: Provides entries in old versions of perl-modules-5.xx
      and libperl5.xx erroneously satisfy dependencies
    - [#899207]\: dist: some script fail syntax checks
* Prepared new Debian package of *dist* package and uploaded to DELAYED/11.
  This newer version is needed to allow perl's build process to continue
rebuilding Configure from its sources: a DFSG requirement.

[perl.debian.net]: http://perl.debian.net/
[Perl-5.28-QA]: https://gobby.debian.org/export/Teams/Perl/Perl-5.28-QA
[#898946]: https://bugs.debian.org/898946
[#898955]: https://bugs.debian.org/898955
[#898989]: https://bugs.debian.org/898989
[#898994]: https://bugs.debian.org/898994
[#899017]: https://bugs.debian.org/899017
[#899075]: https://bugs.debian.org/899075
[#899110]: https://bugs.debian.org/899110
[#899207]: https://bugs.debian.org/899207

Versioned Provides
------------------

Deploying versioned provides in *src:perl* would simplify numerous
dependencies. For instance, *perl* could `Provides: libtest-simple-perl
(= 1.xxxxxx)` and other packages could then only `(Build-)Depends:
libtest-simple-perl (>= 1.xxxxxx)`
without needing an alternative dependency on *perl*. See
*debian-policy* bug ([#761219]).

There have been some hurdles with this work, but currently the last
missing piece is support in autopkgtest ([#867081]). This issue was
worked on during the sprint and a work-in-progress [autopkgtest patch]
was submitted to the autopkgtest maintainers.

[#761219]: https://bugs.debian.org/761219
[#867081]: https://bugs.debian.org/867081
[autopkgtest patch]: https://salsa.debian.org/ci-team/autopkgtest/merge_requests/15

autopkgtest for src:perl
------------------------

While the [autopkgtest-pkg-perl] framework is now widely adopted in
Perl packages, src:perl itself has not had any autopkgtest checks until
now. This was [fixed] during the sprint, resulting in a small collection
of runtime checks that mostly involve things outside the package that
might plausibly cause regressions in perl functionality. We now check
for instance that we can still read Storable and NDBM files generated
on jessie and stretch, and that the Debian archive has not acquired new
"dual life" module packages that would need metadata changes in perl.

[autopkgtest-pkg-perl]: https://perl-team.pages.debian.net/autopkgtest.html
[fixed]: https://ci.debian.net/packages/p/perl/

* implemented rudimentary autopkgtest checks in src:perl
* uploaded in 5.26.2-4
* further improvements pending for 5.26.2-5


Packaging
=========

* *dh-make-perl*, *pkg-perl-tools*: fix all kinds of URLs to point to
  `salsa.debian.org` and upload new releases.
* New packages (2): libb-debug-perl, libpdl-vectorvalued-perl
* New upstream releases (21): libsub-quote-perl, libmodule-cpanfile-perl,
  libimporter-perl, libcryptx-perl, libmetacpan-client-perl,
  libversion-perl, libtest-pod-perl, libfile-homedir-perl,
  libbusiness-issn-perl, libmojolicious-perl, libgitlab-api-v4-perl,
  libfile-copy-recursive-perl, libmath-clipper-perl, libnet-dns-sec-perl,
  libxml-compile-wsdl11-perl, libsoap-lite-perl, libxml-compile-tester-perl,
  libbson-perl, libio-stream-perl, libtest-bdd-cucumber-perl,
  libtest-tempdir-tiny-perl
* Check [removal candidates] and file bugs:
    - [#898986]\: RM: libio-socket-multicast6-perl -- ROM; buggy, outdated,
      unused
    - [#898987]\: RM: libsub-current-perl -- ROM; obsolete with perl >= 5.16.0

[removal candidates]: https://udd.debian.org/cgi-bin/bts-usertags.cgi?tag=rm-candidate&user=debian-perl%40lists.debian.org
[#898986]: https://bugs.debian.org/898986
[#898987]: https://bugs.debian.org/898987


QA work
=======

* Small tweaks to the `get-ci-failures` script.
* Cleanup [OpenTasks] wiki page (remove done tasks, update links, annotate
  problems coming from Alioth → Salsa).
* Update subscription to team packages on *Launchpad*.
* [Website]\: updates for Alioth → Salsa, fixes and editorial changes, small
  improvements in build scripts.
* Even more cleanups of [wiki.d.o Perl pages] beneath
  https://wiki.debian.org/Teams/DebianPerlGroup/
  mostly for the Alioth → Salsa move.
* *pkg-perl-autopkgtests*: improve `syntax.t` to not skip tests on Suggests
  when `syntax-skip` exists, and update affected packages in git.

[OpenTasks]: https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks
[Website]: https://perl-team.pages.debian.net/
[wiki.d.o Perl pages]: https://wiki.debian.org/FrontPage?action=fullsearch&context=180&value=Perl&titlesearch=Titles


Other tasks
===========

* Respond to a packaging request on the mailing list.
* Review and write up [notes from the Perl 5 Hackathon].

[notes from the Perl 5 Hackathon]: https://github.com/p5h/2017/wiki/Debian-Patch-Review
