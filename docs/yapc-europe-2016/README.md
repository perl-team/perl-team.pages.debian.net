From Debian with ♥
==================

Debian is one of the largest and oldest Free Operating Systems.
The Debian Perl Group makes CPAN distributions available to users
of Debian and its derivates as Debian packages.

This [talk](http://act.yapc.eu/ye2016/talk/6770) gives an overview
of the group's work, and presents some ideas to the Perl community
how CPAN authors can make our work even easier.

Copyright and License
---------------------

*   © 2016, Alex Muntada
    CC BY-SA 4.0
    https://pkg-perl.alioth.debian.org/docs/yapc-europe-2016/
*   Based on:
    "From Debian with ♥"
    © 2014, gregor herrmann
    CC BY-SA 4.0
    https://pkg-perl.alioth.debian.org/docs/barcelona-perl-workshop-2014/
*   "use Debian" logo:
    © 2009, ghostbar
    CC BY-NC-SA 2.0
*   Parts from:
    "How to maintain 2500 Perl Modules as Debian Packages"
    © 2013, Axel Beckert
    CC SA 3.0 DE
    http://pkg-perl.alioth.debian.org/docs/swiss-perl-workshop-2013/

