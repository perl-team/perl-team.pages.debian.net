% Debian Perl Rolling Sprint at DebCamp 2018

## Introduction

The Debian Perl Group conducted a "Rolling Sprint" during DebCamp 2018
in Hsinchu, Taiwan from July 21 to July 27.

The term "Rolling Sprint" describes an event that extended over the whole
DebCamp week with the opportunity for people to "hop on" and "hop off" for
one or more days. This made it possible for persons who had more plans
for DebCamp than one single area of work to participate according to their
availability.

The structure of the "Rolling Sprint" was:

* A(n almost) daily coordination meeting after breakfast, where people
  coordinate the work for this day;
* people working on their selected tasks over the day, alone, in pairs, in
  groups 

Altogether 4 people took part in one or more of the coordination meetings
and/or work sessions: bremner, intrigeri, kanashiro, nodens.
Additionally, a few other team members participated remotely in the
sprint (mostly asynchronously due to timezone differences).

The participants would like to thank the [sponsors][] of DebConf18 and the
DebConf Team for making this sprint possible.

[sponsors]: https://debconf18.debconf.org/sponsors/

## New releases of our software

We have released dh-make-perl 0.102 and pkg-perl-tools 0.46.

## Reproducible builds

* triaged and determined root cause: libcrypt-openssl-rsa-perl,
  libvariable-magic-perl
* investigated: libmarc-charset-perl

## Candidates for removal

* Identified a few new candidates for removal and let upstream know
  about our plans:
  - libdevel-beginlift-perl
  - libmail-deliverystatus-bounceparser-perl
  - libpoe-component-client-mpd-perl
  - libtest-aggregate-perl
* shutter and its `lib{gnome,gtk}2-*-perl` dependencies: one year after DebCamp17,
  where the removal process started, asked when we can complete it, agreed on
  removing stuff now (https://bugs.debian.org/870418#117).
  Requests for removal:
  - [#904526](https://bugs.debian.org/904526) (shutter)
  - [#904531](https://bugs.debian.org/904531) (libgnome2-perl)
  - [#904534](https://bugs.debian.org/904534) (libgnome2-vfs-perl)
  - [#904535](https://bugs.debian.org/904535) (libgnome2-canvas-perl)
  - [#904539](https://bugs.debian.org/904539) (libgnome2-gconf-perl)
  - [#904540](https://bugs.debian.org/904540) (libgnome2-wnck-perl)
  - [#904538](https://bugs.debian.org/904538) (libgtk2-imageview-perl)
  - [#904541](https://bugs.debian.org/904541) (libgtk2-unique-perl)
* Investigated why we still have so many other `lib{gnome,gtk}2-*-perl` packages,
  filed bugs against them and reverse-dependencies, and (when relevant) got in
  touch with upstream:
  - libgtk2-ex-entry-pango-perl
  - libgtk2-ex-podviewer-perl
  - libgtk2-ex-printdialog-perl
  - libgtk2-ex-simple-list-perl
  - libgtk2-ex-volumebutton-perl
  - libgtk2-gladexml-perl
  - libgtk2-gladexml-simple-perl
  - libgtk2-notify-perl
  - libgtk2-spell-perl
  - libgtk2-sourceview2-perl
  - libgtk2-trayicon-perl
  - libgtk2-traymanager-perl
  - xacobeo

## Miscellaneous

* We have triaged or fixed a few bugs:
  - [#704527](https://bugs.debian.org/704527)
  - [#904197](https://bugs.debian.org/904197)
  - [#852848](https://bugs.debian.org/852848)
* Updated upstream Git repos for
  <https://gitlab.gnome.org/GNOME/perl-extutils-pkgconfig>
* Checked for "team"-maintained packages in unstable which are not in
  Git: apparently none.
* Run `dpt salsa configurerepo --all` to make sure all repos on salsa
  are configured correctly, and send mail to the list about using
  `dpt-salsa pushrepo` etc.
* Found `git-dpm` using packages and emailed the list about
  "converting" them or having a discussion first.
* Emailed the list about failing and probably untested autopkgtests.
* Reorganized
  <https://wiki.debian.org/Teams/DebianPerlGroup/Infrastructure> to
  show current state + some history for missing stuff after Alioth
  retirement. There are some FIXME's but it should give an up to date
  picture of the team infrastructure now.
* Updated Launchpad subscriptions for pkg-perl maintained packages.

## New upstream releases

We have uploaded at least 53 new upstream releases:

* libbenchmark-timer-perl
* libdevel-cover-perl
* libwww-youtube-download-perl
* libcpanel-json-xs-perl
* libwww-mechanize-gzip-perl
* libgetopt-lucid-perl
* libio-async-loop-epoll-perl
* libgeo-coordinates-osgb-perl
* libsearch-xapian-perl
* libnet-dns-perl
* libtcl-perl
* libfile-sharedir-perl
* libarchive-tar-wrapper-perl
* carton
* libtype-tiny-perl
* libexporter-tiny-perl
* libanyevent-dbi-perl
* libparse-edid-perl
* liblinux-epoll-perl
* libcompress-raw-lzma-perl
* libfilehandle-unget-perl
* libtest-poe-client-tcp-perl
* libparallel-forkmanager-perl
* libbusiness-ismn-perl
* libtest-cleannamespaces-perl
* libfile-which-perl
* libcpan-perl-releases-perl
* libperl-critic-perl
* libtext-simpletable-perl
* rename
* libpod-projectdocs-perl
* sqitch
* get-flash-videos
* libnet-dns-perl
* libtcl-perl
* libstring-crc32-perl
* libnet-amazon-s3-perl
* libhttp-exception-perl
* libquantum-superpositions-perl
* libclass-accessor-grouped-perl
* libmouse-perl
* libjavascript-beautifier-perl
* libmojolicious-perl
* libclass-date-perl
* libmojo-rabbitmq-client-perl
* libperl-critic-policy-variables-prohibitlooponhash-perl
* libmath-utils-perl
* libtext-quoted-perl
* libmodule-faker-perl
* liblog-report-perl
* libdist-zilla-plugin-run-perl
* libdist-zilla-plugin-repository-perl
* libjira-rest-perl

## New packages

We have uploaded at least 4 new packages:

* libconvert-scalar-perl
* libanyevent-fork-perl
* libproc-fastspawn-perl
* libio-fdpass-perl
