Debian Perl Group Rolling Sprint at DebCamp 2015
================================================

Introduction
------------

The Debian Perl Group conducted a "Rolling Sprint" during DebCamp 2015 in
Heidelberg from 2015-08-10 - 2015-08-14.

The term "Rolling Sprint" describes an event that extended over the whole
DebCamp week with the opportunity for people to "hop on" and "hop off" for
one or more days. This made it possible for persons who had more plans
for DebCamp than one single area of work to participate according to their
availability.

The structure of the "Rolling Sprint" was:

* A daily meeting after breakfast, where people coordinate the work for this
  day;
* people working on their selected tasks over the day, alone, in
  pairs, in groups; 
* short reports about the achieved work in the next morning meeting.

Altogether 8 people took part in one or more of the coordination meetings
and/or work sessions: XTaran, ansgar, bremner, carnil, fsfs, gregoa,
intrigeri, kanashiro.

The participants would like to thank the [sponsors][] of DebConf15 and the
DebConf Team for making this sprint possible.

[sponsors]: http://debconf15.debconf.org/sponsors.xhtml


git and patches - git-debcherry
===============================

Following up in the discussions and work done at the [pkg-perl BoF at
DebConf14][BOF2014] and the [Debian Perl Sprint in May 2015][SprintBCN2015],
more work was done on experimenting with our workflow regarding patches in
git, especially around git-debcherry.

[BOF2014]: https://lists.debian.org/debian-perl/2014/08/msg00044.html
[SprintBCN2015]: https://lists.debian.org/debian-perl/2015/07/msg00009.html

On Tuesday afternoon, bremner gave a demo of git-debcherry in combination
with ntyni's [tools][].

[tools]: http://anonscm.debian.org/cgit/pkg-perl/scripts.git/tree/debcherry

A newer git-debcherry with improvements (especially around git-notes) was
uploaded during DebCamp, fixing two bugs:

* [#784130](https://bugs.debian.org/784130): ping maintainer, patch seems ready.
* [#784159](https://bugs.debian.org/784159): style updates to patch, sign-off on new feature.

In the following discussion, several issues and
questions were raised:

* Minor glitches in the scripts, which were fixed later in the week by fsfs.
* The main question is still: How to work on a git-debcherried package
  without buying into git-debcherry?
* Brainstorming: commit patches on a specific branch (or on master), allow
  people to work there, create tooling to import changes / new patches from
  this branch back into master (at `dpt checkout` time?). Maybe with a special
  header "X-Generated-By: git-debcherry" to filter?
* If we had a patch branch:
  - when/how to write to this branch? when: commit at sign/tag/upload time
  - when/how to re-import into master manually created patches?
* gbp doesn't fetch notes yet (bug report by XTaran, bremner will write a
  patch): [#786607](https://bugs.debian.org/786607)
    - in the meantime, `dpt-checkout` and our `.mrconfig` fetch the notes
      refs

In the following days fsfs converted two packages to use git-debcherry and
on the way improved the [tools][]. The exported quilt patches are committed
on master in debian/patches, so the git checkout is identical to the source
package with no magic going on at build time. Working on one of those packages
with quilt while ignoring git-debcherry requires an extra step to make the
already-applied patches known to quilt, and may in turn force the next
developer to take additional measures to reintegrate that work with
git-debcherry. The need for both is readily apparent, however, and no work
is lost permanently, so this looks like a promising way to a new, git-centric
workflow.


Work on individual packages
===========================

* [#732725](https://bugs.debian.org/732725) (libogre-perl FTBFS) →
  [#795067](https://bugs.debian.org/795067) (RM; RoM) [abe]
* [#794963](https://bugs.debian.org/794963) (Net::XMPP warnings under setuid/root): Tried to reproduce [abe]
* libdatetime-timezone-perl: update to Olson db 2015f in sid, jessie, wheezy
* [#791507](https://bugs.debian.org/791507): new upstream version [bremner], uploaded to Debian [carnil]
* Update liburi-perl and libcatalyst-perl (fixes an RC bug).
* libglib-perl: cleaned up patches, split some, and forwarded them all upstream.
* dh-dist-zilla, libdist-zilla-perl, etc.


QA work across packages
=======================

* Update *many* packages to new upstream versions. [kanashiro]
* Run [DUCK](http://duck.debian.net/) over all packages.

Bug triaging
------------

* Forward bugs reports upstream.
* Ping upstream bug reports for RC bugs / Perl 5.22 transition problems.
* Report new bugs from going through the [reproducible build logs][rb].
* Go through [ci.debian.net][ci] failures, fix packages, file bugs.

[rb]: https://reproducible.debian.net/unstable/amd64/pkg_set_maint_pkg-perl-maintainers.html
[ci]: http://ci.debian.net/


PET/repository maintenance
--------------------------

- missing tags: ping maintainers to push missing tags/branches to git.deban.org
- repository cleanup: ping maintainers about packages which are in git but never uploaded

Unification across packages
---------------------------

- unify paths in lintian overrides for multi-arch paths in hardening-no-fortify-functions tag
- attempt to unify uversionmangle in d/watch: cancelled, too much variance, no clear "best" option, too little gain
- attempt to switch from repacking framework to Files-Excluded: cancelled, mk-origtargz errors out too often
- unify environment variables for disabling network tests (debian/rules and debian/patches)
- unify TEST_FILES filtering in debian/rules

pkg-perl-tools
--------------

- update examples/check-build: run adt-run; manipulate .changes with mergechanges; add support for adt-qemu
- patchedit: support $EDITOR containing options or parameters [abe/carnil]
- dpt-import-orig: check also for upstream tags with padding zeros removed

lintian
-------

Since 2013, we have a team-specific lintian profile. During the sprint, XTaran
moved some of the checks, which are relevant for others as well, to lintian
proper.


Statistics
==========

* Andreas Tille pointed out (both in private and in one of his talks) that
  pkg-perl is exceptional in its distribution of [number of maintainers per
  package](http://blends.debian.net/liststats/maintainer_per_package_pkg-perl.png).
* During DebCamp and DebConf (IRC log from 2015-08-10T00:00+02:00 -
  2015-08-23T23:59:59+02:00) 189 packages were uploaded, closing 19 bugs.

Resources
=========

* Announcement:
  [Wiki](https://wiki.debian.org/Sprints/2015/DebianPerlDebCamp),
  [email](https://lists.debian.org/debian-perl/2015/03/msg00041.html)
* Notes:
  [Gobby](infinote://gobby.debian.org/Teams/Perl/Team-Sprint-Debcamp-2015)
