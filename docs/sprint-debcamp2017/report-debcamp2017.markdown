Debian Perl Group Rolling Sprint at DebCamp 2017
================================================

Introduction
------------

The Debian Perl Group conducted a "Rolling Sprint" during DebCamp 2017 in
Montréal from 2016-07-31 - 2017-08-04.

The term "Rolling Sprint" describes an event that extended over the whole
DebCamp week with the opportunity for people to "hop on" and "hop off" for
one or more days. This made it possible for persons who had more plans
for DebCamp than one single area of work to participate according to their
availability.

The structure of the "Rolling Sprint" was:

* A daily coordination meeting after breakfast, where people
  coordinate the work for this day;
* people working on their selected tasks over the day, alone, in pairs, in
  groups;
* a progress report at the next meeting to summarize and document the results. 

Altogether N people took part in one or more of the M coordination meetings
and/or work sessions: x1, x2, ....

The participants would like to thank the [sponsors][] of DebConf17 and the
DebConf Team for making this sprint possible.

[sponsors]: https://debconf17.debconf.org/sponsors/


Resources
=========

* Announcement:
  [Wiki](https://wiki.debian.org/Sprints/2017/DebianPerlDebCamp),
  [email](https://lists.debian.org/debian-perl/2017/07/msg00008.html)
* Notes:
  [Gobby](infinote://gobby.debian.org/Teams/Perl/Team-Sprint-Debcamp-2017)
  [GobbyWeb](https://gobby.debian.org/export/Teams/Perl/Team-Sprint-Debcamp-2017)
