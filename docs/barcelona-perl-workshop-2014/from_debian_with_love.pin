#!/usr/bin/pinpoint -s

[font=Sans 50px]
[transition=text-slide-up]
[duration=45]

[debian_perl.jpg]
#[white]
[text-color=white]
[shading-opacity=1.0]
[fit]

-- [text-align=center]
<b>From Debian, with ♥</b>

gregor herrmann

<i>Barcelona Perl Workshop, 2014-11-08</i>

--

<b>Outline</b>

♥ About Me
♥ Debian and the Debian Perl Group
♥ How to make our lives even easier

♥ Maintaining Perl distributions as
    Debian packages

--

<b>Me =</b>

♥ gregor herrmann,
    gregoa@{debian,cpan}.org
    gregoa on IRC, identi.ca, etc.
♥ Innsbruck, Austria
♥ member of the Debian Perl Group 
    (since 2006)
♥ Debian Developer 
    (since 2008)

--

<b>Debian</b>

♥ Free Operating System
♥ a.k.a. "Linux Distribution"
♥ since 1993
♥ 22.000 source packages, 
    45.000 binary packages

<i>Who is using Debian here?</i>

--

<b>Debian Perl Group</b>

♥ since 2004
♥ Aim:
    We want to constantly improve 
    the coverage of available Perl modules 
    in Debian.
♥ ~3.000 packages
    [of ~3.500 lib.*-perl in total]
♥ 254 members [in theory]
♥ 54 committers since 2014-01-01
♥ We don't maintain perl itself


-- [font=Monospace 100px]


I believe that the core, beautiful, exciting thing that we
do inside Debian, and that any other excellent Linux
distribution does, is exactly accepting what upstream does.
Not accepting in the sense of passive apathy, but in the
sense of wholehearted embrace of upstream's ideas,
expertise, passion, and hard work, and finding a way to
incorporate that into our distribution. Acceptance in the
sense of reaching out with both hands and taking hold of
the gift we are given with a firm grasp and a grateful
heart.

Linux distributions are *all about* upstreams. Those
upstreams are the reason why we're here.

<i>Russ Allbery</i>

--

<b>Debian Perl Group and the CPAN</b>

My claim: we are quite similar

♥ Kinda relaxed, cooperative, helpful, friendly
♥ Focus on quality and Doing It Right™
♥ Solutions before ego boosting (teamwork)

<i>Who maintains distributions on the CPAN?</i>

-- [text-align=center]

<b>Some messages to 
the CPAN community</b>

-- [text-align=center]

<b>
Moltes gràcies!

¡Muchas gracias!

Thanks a lot!
</b>

-- [text-align=center]

<b>And what can CPAN 
authors do to make 
our lives even easier?</b>

--

♥ Keep up the good work!
♥ Make good, state-of-the-art
    CPAN distributions
♥ Use current tools,
    follow best practices

-- [text-align=center]

<b>Some details …</b>

--

<b>Changes</b>

♥ Include a Changes file
♥ Changes != $vcs log
♥ not "fix RT#123, merge PR #456"

--

<b>Bugs</b>

♥ mention CPAN ticket id / github issue id 
    in Changes
♥ mention preferred bug tracker in META.*

--

<b>Git</b>

♥ Nice!
♥ Please tag, (sign tags,)  and:
    push tags!

--

<b>Tests</b>

♥ variables (RELEASE_TESTING, 
    AUTOMATED_TESTING), author tests, 
    xt/, …
    (cf. Lancaster consensus)
♥ in Debian: no internet access 
    allowed / guaranteed
    maybe Test::RequiresInternet helps?
♥ personal recommendation: 
    use Test::Spelling
♥ don't assume source directory, blib/,
    uid, .git/, …

--

<b>{Makefile,Build}.PL</b>

♥ No prompts
♥ or ExtUtils::MakerMaker::prompt()
♥ Also think of the CPAN smoke testers

--

<b>Tarballs</b>

♥ No debian/ directory
♥ or .gitignore
♥ or tempfiles / backups files

--

<b>System libs</b>

♥ Including external (C) libraries
    might be needed for other OSes
♥ but please make it easy for us
    to use the system libs
    (configure switch, env variable, ...)

--

<b>inc/ is evil</b>

♥ Code duplication, and …
♥ bug duplication!

♥ And embedded <u>minified</u> JavaScript
    files are evil as well
    (not the preferred form of modification)

--

<b>Requirements</b>

♥ Minimum required versions …
♥ … not what is installed locally / 
    what is in bleadperl

♥ "47.11 → 0" ?!
♥ 0 -> '0' -> 0 -> '0' -> … ?!

--

<b>Versions</b>

♥ Use a consistent versioning scheme
♥ CPAN: 0.10 &lt; 0.1001 &lt; 0.11
♥ Debian: 0.10 &lt; 0.1001 <span foreground='red'>&gt;</span> 0.11
    ✔ 0.1001 → 0.10.01 (or else:)
    ✔ 0.11     → 0.1100, 1:0.11

--

<b>Legal stuff</b>

♥ We need copyright/licenses
♥ free software licenses
♥ consistent &amp; complete

♥ Again, modern tools, like Dist::Zilla

--

<b>And now: our workflow / tools / …</b>

--

<b>Links</b>

https://wiki.debian.org/Teams/DebianPerlGroup/
https://wiki.debian.org/Teams/DebianPerlGroup/Welcome
https://pkg-perl.alioth.debian.org/

https://wiki.debian.org/UpstreamGuide

--

<b>Communication</b>

♥ 3 mailing lists
♥ IRC (#debian-perl @ OFTC)
♥ meetings in meatspace (DebConf, maybe a sprint?)

--

<b>Code</b>

♥ Packages are maintained in Git
    (svn before Summer 2011)
♥ https://anonscm.debian.org/cgit/pkg-perl/
♥ KGB sends notifications to IRC

--

<b>Tools</b>

♥ lots of them
♥ some generic Debian tools
♥ some (pkg-)perl specific

--

<b>PET</b>

♥ Package Entropy Tracker
♥ http://pet.debian.net/pkg-perl/pet.cgi
♥ VCS-centered view on packages showing 
    what to do: new upstream release, bugs, 
    pendings uploads, …

--

<b>dh-make-perl</b>

♥ create (or update) Debian package 
    from CPAN tarball
♥ cpan2deb =~ dh-make-perl … --build

♥ Foo::Bar → libfoo-bar-perl

♥ Result: a Debian source package,
    i.e. an unpacked tarball + debian/ directory
    containing meta information and build
    instructions
--

<b>pkg-perl-tools</b>

♥ collection of helpers
♥ CLI called as `dpt …'
    manipulating our Git repos
    forwarding bug reports and patches
    combining upstream Git repos with ours

--

<b>Track your packages!</b>

♥ Package Tracking System:
    https://packages.qa.debian.org/libFOO-BAR-perl
    https://tracker.debian.org/pkg/libFOO-BAR-perl
    subscription to event types available
♥ Bug Tracking system:
    https://bugs.debian.org/libFOO-BAR-perl
♥ Git repo:
    http://anonscm.debian.org/cgit/pkg-perl/packages/libFOO-BAR-perl.git
♥ Build logs (XS modules):
    https://buildd.debian.org/libFOO-BAR-perl

--

<b>Addendum</b>

♥ Copyright &amp; license:
    © 2014, gregor herrmann
    CC BY-SA 4.0
♥ "use Debian" logo:
    © 2009, ghostbar
    CC BY-NC-SA 2.0
♥ Parts from
    "How to maintain 2500 Perl Modules as Debian Packages"
    © 2013, Axel Beckert
    CC SA 3.0 DE
    http://pkg-perl.alioth.debian.org/docs/swiss-perl-workshop-2013/
♥ Download:
    http://pkg-perl.alioth.debian.org/docs/barcelona-perl-workshop-2014/

-- [text-align=center]

Summary:

<b>
Moltes gràcies!

¡Muchas gracias!

Thanks a lot!
</b>

